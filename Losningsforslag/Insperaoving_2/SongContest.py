# Del 1
import random

songs = [("You hear the door slam. And realize there's nowhere left to", "run"),
         ("Oh, I wanna dance with somebody. I wanna feel the",  "heat"),
         ("There's a fire starting in my heart. Reaching a fever", "pitch"),
         ("Hey, I just met you and this is crazy. But here's my", "number"),
         ("'Cause baby, you're a firework. Come on, show 'em what you're", "worth")]

def pop_random_song(songs: list):
    index = random.randint(0, len(songs)-1)
    return songs.pop(index)


# Del 2
def get_guess():
    return input("What is the next word? ")

def song_contest(songs):
    while True:
        song = pop_random_song(songs)
        print("The lyrics are:")
        print(song[0])
        guess = get_guess()
        while guess.lower() != song[1]:
            print("Wrong guess. Try again.")
            guess = get_guess()
        print("Correct!")
        if not len(songs):
            print("Congratulation, music lover! You have managed to get every song lyric we had correct!")
            break
        one_more = input("Do you want to go again? (y/n) ")
        if one_more.lower() == 'n':
            print("Welcome back later :D")
            break


# song_contest(songs)
