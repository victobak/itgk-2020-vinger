# Del 1
def derivation(value,f):
    h = 1e-8
    limit = (f(value+h)-f(value))/h
    return round(limit,3)


# Del 2
def polynom(x):
    return x**2+2*x+13

print(derivation(3,polynom))
